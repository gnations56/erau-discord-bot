FROM ubuntu
RUN apt install -y libffi-dev libnacl-dev python3-dev python3-pip python3 git
RUN git clone "https://gitlab.com/gnations56/erau-discord-bot.git" "root/discordbot"
RUN pip3 install -r /root/discordbot/requirements.txt
CMD "python3 /root/discordbot/bot.py"
