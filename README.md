# ERAU Discord Bot

This is the source repo for the ERAU Discord Server bot

### Configuration
To configure a plugin, view it's respective page on the [wiki]().  
For a starter configuration, rename config.toml.example to config.toml and enter your bot token in the `bot_token` variable.
