import toml


class TOMLConfig():
    def __init__(self, file):
        self.key = ""
        self.file = file
        self.config_dict = {}

    def load_config(self):
        self.config_dict = toml.load(self.file)

    def set_config_block(self, key):
        self.key = key

    def get_config_option_from_block(self, option):
        if option in self.config_dict[self.key]:
            return self.config_dict[self.key][option]
        else:
            return False
