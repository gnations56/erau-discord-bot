import minecraft

def create_auth_token(user, password):
    return minecraft.authentication.AuthenticationToken().authenticate(user, password)


def create_connection(server_address, server_port, auth_token):
    return minecraft.Connection(server_address, server_port, auth_token)


def chat_message_handler(chat_packet):
    return chat_packet.json_data
